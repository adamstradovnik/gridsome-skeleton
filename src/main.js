// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: https://gridsome.org/docs/client-api

import '~/assets/scss/styles.scss'
import DefaultLayout from '~/layouts/Default.vue'
import gridsomeConfig from './../gridsome.config.js'

export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)

  const siteInfo = {
    title: 'Site Skeleton',
    keywords: 'gridsome, skeletom, template, site',
    description: 'Gridsome simple site skeleton',
    image: '#',
    twitterHandle: ''
  }

  head.htmlAttrs = { lang: 'sl' }
  head.bodyAttrs = { class: 'antialiased font-body font-serif' }

  // Styles
  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css?family=Great+Vibes|Libre+Baskerville&display=swap'
  })

  // Basic meta tags
  head.meta.push({
    name: 'keywords',
    content: siteInfo.keywords
  })

  head.meta.push({
    name: 'description',
    content: siteInfo.description
  })

  // Open Graph + Twitter meta tags
  head.meta.push({
    key: 'og:locale',
    name: 'og:locale',
    content: 'sl_SI'
  })

  head.meta.push({
    property: 'og:description',
    content: siteInfo.description
  })

  head.meta.push({
    name: 'twitter:description',
    content: siteInfo.description
  })

  head.meta.push({
    property: 'og:type',
    content: 'website'
  })

  head.meta.push({
    property: 'og:title',
    content: siteInfo.title
  })

  head.meta.push({
    name: 'twitter:title',
    content: siteInfo.title
  })

  head.meta.push({
    name: 'twitter:card',
    content: 'summary_large_image'
  })

  head.meta.push({
    name: 'twitter:creator',
    content: siteInfo.twitterHandle
  })

  head.meta.push({
    property: 'og:image',
    content: siteInfo.image
  })

  head.meta.push({
    name: 'twitter:image',
    content: siteInfo.image
  })

  router.beforeEach((to, from, next) => {
    head.meta.push({
      key: 'og:url',
      name: 'og:url',
      content: gridsomeConfig.siteUrl + to.path
    })
    next()
  })
}
